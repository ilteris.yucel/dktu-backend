const dbPool = require('../base/db');
const utils = require('../base/utils');

const getAllTestResults = (callback) => {
    dbPool.query('SELECT * from test_results', callback);
};

const getOneTestResult = (result_id, callback) => {
    dbPool.query('SELECT * FROM test_results WHERE result_id = $1', [result_id], callback);
};

const getPatientTestResults = (patient_id, callback) => {
    dbPool.query('SELECT * FROM test_results WHERE patient_id = $1', [patient_id], callback);
};

const addTestResult = (test_result, callback) => {
    dbPool.query('INSERT INTO test_results (result_id, patient_id, test_name, result, is_adaptive, is_noisy) VALUES ($1, $2, $3, $4, $5, $6) RETURNING *', Object.values(test_result), callback);
};

const deleteTestResult = (result_id, callback) => {
    dbPool.query('DELETE FROM test_results WHERE result_id = $1 RETURNING *', [result_id], callback);
};

const deletePatientTestResults = (patient_id, callback) => {
    dbPool.query('DELETE FROM test_results WHERE patient_id = $1 RETURNING *', [patient_id], callback);
}

module.exports = {
    getAllTestResults,
    getOneTestResult,
    getPatientTestResults,
    addTestResult,
    deleteTestResult,
    deletePatientTestResults,
};