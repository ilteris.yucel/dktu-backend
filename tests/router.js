const express = require('express');
const router = express.Router();
const controllers = require('./controllers');

router.param('name', (req, res, next, name) => {
    req.nameFromParam = name;
    next();
});

router.get('/', controllers.getAllTests);
router.get('/list', controllers.getTestList);
router.get('/sound', controllers.getAllSounds);
router.get('/image', controllers.getAllImages);
router.get('/:name', controllers.getOneTest);
router.get('/sound/list', controllers.getSoundList);
router.get('/sound/:name', controllers.getOneSound);
router.get('/image/list', controllers.getImageList);
router.get('/image/:name', controllers.getOneImage);


module.exports = router;
