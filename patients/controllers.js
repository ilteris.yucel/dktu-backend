const models = require('./models');
const { v4: uuidv4 } = require('uuid');

const getAllPatients = (req, res, next) => {
    models.getAllPatients((error, result) => {
        if(error)
            return res.status(406).json({'status' : 'failed', 'data' : error });
        res.status(200).json({'status' : 'success', 'data' : result.rows});
    });
};

const getOnePatient = (req, res, next) => {
    const { id } = req.params;
    if(!id)
        return res.status(400).json({'status' : 'failed', 'data' : 'invalid id' });
    models.getOnePatient(id, (error, result) => {
        if(error)
            return res.status(406).json({'status' : 'failed', 'data' : error });
        res.status(200).json({'status' : 'success', 'data' : result.rows});
    });
};

const addPatient = (req, res, next) => {
    const createdOn = new Date();
    if(!(
        req.body.name && req.body.surname 
        && req.body.birth_date && req.body.age
        && req.body.gender && req.body.file_number
        && req.body.phone_number && req.body.city
        && req.body.mother_edu_level && req.body.father_edu_level
    )){
        return res.status(400).json({'status' : 'failed', 'data' : 'some required fields are missing'});
    }
    const patient = {
        'id' : uuidv4(),
        ...req.body,
        'created_on': createdOn,
    };
    models.addPatient(patient, (error, result) => {
        if(error)
            return res.status(406).json({'status' : 'failed', 'data' : error });
        res.status(201).json({'status' : 'success', 'data' : result.rows});        
    });
};

const deletePatient = (req, res, next) => {
    const { id } = req.params; 
    if(!id)
        return res.status(400).json({'status' : 'failed', 'data' : 'invalid id' });
    models.deletePatient(id, (error, result) => {
        if(error)
            return res.status(406).json({'status' : 'failed', 'data' : error });
        res.status(200).json({'status' : 'success', 'data' : result.rows});  
    });
};

const updatePatient = (req, res, next) => {
    const { id } = req.params; 
    if(!id)
        return res.status(400).json({'status' : 'failed', 'data' : 'invalid id' });
    const patient = {
        'id': id,
        ...req.body,
    };
    models.updatePatient(patient, (error, result) => {
        if(error)
            return res.status(406).json({'status' : 'failed', 'data' : error });
        res.status(200).json({'status' : 'success', 'data' : result.rows});  
    });
};

module.exports = {
    getAllPatients,
    getOnePatient,
    addPatient,
    deletePatient,
    updatePatient
};