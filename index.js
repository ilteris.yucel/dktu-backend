const express = require('express');
const cors = require('cors');
const path = require('path');
const PORT = process.env.PORT || 3000;
global.appRoot = path.resolve(__dirname);
const corsOptions = {
  origin: '*'
};

const app = express();
const patientRouter = require('./patients/router');
const userRouter =  require('./users/router');
const testRouter = require('./tests/router');
const testResultsRouter = require('./test_results/router');

app.use(cors(corsOptions));
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use('/users', userRouter);
app.use('/patients', patientRouter);
app.use('/tests', testRouter);
app.use('/test-results', testResultsRouter);

app.get('/', (req, res) => {
    res.send('ROOT APP ROOT');
});



app.listen(PORT, () => {
    console.log(`Server is running on port ${PORT}...`);
});
  
app.use('/assets', express.static(path.join(__dirname, 'assets')));
  